<?php

namespace App\Controller;


use App\Entity\Conge;
use App\Entity\Developer;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class CongeController extends AbstractController
{
    /**
     * @Route("/api/admin/conges", name="liste_congés", methods={"GET"})
     */
    public function index(): Response
    {
        $conges = $this->getDoctrine()->getRepository(Conge::class)->findAllArray();
        // dd($conges);
        /* 
        $congeByDev = array();
        foreach ($conges as $conge) {
            $developerName = $conge["developer"]["fname"];

            $congeByDev[$developerName][] = $conge;
        } */


        return $this->json([
            "code" => 200,
            "data" => $conges
        ]);
    }

    /**
     * @Route("/api/conges/{id}", name="show_conge", methods={"GET"})
     */
    public function show($id): Response
    {
        $em = $this->getDoctrine()->getManager();
        $conge = $em->getRepository(Conge::class)->findOneArray($id);

        return $this->json([
            "code" => 200,
            "data" => $conge[0]
        ]);
    }

    /**
     * @Route("/api/user/conges", name="lister_conge", methods={"GET"})
     */
    public function getMyConge(EntityManagerInterface $em): Response
    {
        $connected_user = $this->getUser();

        $devId = $connected_user->getDeveloper()->getId();

        $conges = $this->getDoctrine()->getRepository(Conge::class)->findMyCongesArray($devId);

        return $this->json([
            "code" => 200,
            "data" => $conges
        ]);
    }


    /**
     * @Route("/api/admin/conges", name="create_conge", methods={"POST"})
     */
    public function create(Request $request)
    {
        $req = json_decode($request->getContent());

        $devId = $req->developer;
        $date = $req->date;
        $nbjour = $req->nbjour;

        $em = $this->getDoctrine()->getManager();

        $developer = $em->getRepository(Developer::class)->find($devId);

        $conge = new Conge();
        $conge->setDeveloper($developer);
        $conge->setDate(new DateTime(date('Y-m-d', strtotime($date))));
        $conge->setNbjour($nbjour);
        $conge->setAccepted(true);

        // tell Doctrine you want to (eventually) save the Product (no queries yet)
        $em->persist($conge);
        // actually executes the queries (i.e. the INSERT query)
        $em->flush();

        return $this->json([
            'code' => 200,
            "msg" => "Conge Created Successfully",

        ]);
    }

    /**
     * @Route("/api/user/conges", name="demande_conge", methods={"POST"})
     */
    public function demande(Request $request)
    {
        $req = json_decode($request->getContent());


        $date = $req->date;
        $nbjour = $req->nbjour;

        $em = $this->getDoctrine()->getManager();
        $connected_user = $this->getUser();

        $devId = $connected_user->getDeveloper()->getId();


        $developer = $em->getRepository(Developer::class)->find($devId);

        $conge = new Conge();
        $conge->setDeveloper($developer);
        $conge->setDate(new DateTime(date('Y-m-d', strtotime($date))));
        $conge->setNbjour($nbjour);
        $conge->setAccepted(false);

        // tell Doctrine you want to (eventually) save the Product (no queries yet)
        $em->persist($conge);
        // actually executes the queries (i.e. the INSERT query)
        $em->flush();

        return $this->json([
            'code' => 200,
            "msg" => "Conge Created Successfully",

        ]);
    }

    /**
     * @Route("/api/admin/conges/{id}", name="accepter_conge", methods={"PUT"})
     */
    public function accepter($id)
    {


        $em = $this->getDoctrine()->getManager();
        $conge = $em->getRepository(Conge::class)->find($id);


        $conge->setAccepted(!$conge->getAccepted());

        $em->flush();

        return $this->json([
            'code' => 200,
            "msg" => "Conge accepted Successfully",

        ]);
    }
}
