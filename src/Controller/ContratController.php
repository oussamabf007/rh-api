<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;


class ContratController extends AbstractController
{
    /**
     * @Route("/api/admin/contrat", name="contrat", methods={"GET"})
     */
    public function createPDFAction(Request $request)
    {

        // $source = $request->get('source');


        /*    $mpdfService = $this->get('tfox.mpdfport');
        //  $html = file_get_contents($source);
        $mpdf = $mpdfService->getMpdf();
        $mpdf->WriteHTML('<h1>Hello world!</h1>');
        $mpdf->Output();
        exit; */
        $mpdf = new \Mpdf\Mpdf();
        $mpdf->WriteHTML('<!doctype html>
                          <title>Site Maintenance</title>
<style>
  body { text-align: center; padding: 150px; }
  h1 { font-size: 50px; }
  body { font: 20px Helvetica, sans-serif; color: #333; }
  article { display: block; text-align: left; width: 650px; margin: 0 auto; }
  a { color: #dc8100; text-decoration: none; }
  a:hover { color: #333; text-decoration: none; }
</style>
<article>
    <h1>We&rsquo;ll be back soon!</h1>
    <div>
        <p>Sorry for the inconvenience but we&rsquo;re performing some maintenance at the moment. If you need to you can always <a href="mailto:#">contact us</a>, otherwise we&rsquo;ll be back online shortly!</p>
        <p>&mdash; The Team</p>
    </div>
</article>');
        $mpdf->Output();
    }
}
