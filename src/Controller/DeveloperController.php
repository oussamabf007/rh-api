<?php

namespace App\Controller;

use App\Entity\Developer;
use DateTime;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class DeveloperController extends AbstractController
{
    /**
     * @Route("/api/admin/developers", name="liste_developpeurs", methods={"GET"})
     */
    public function index(): Response
    {
        $developers = $this->getDoctrine()->getRepository(Developer::class)->findAllArray();

        return $this->json([
            "code" => 200,
            "data" => $developers
        ]);
    }

    /**
     * @Route("/api/developers/{id}", name="show_developer", methods={"GET"})
     */
    public function show($id): Response
    {
        $em = $this->getDoctrine()->getManager();
        $developer = $em->getRepository(Developer::class)->findOneArray($id);

        return $this->json([
            "code" => 200,
            "data" => $developer[0]
        ]);
    }

    /**
     * @Route("/api/developers", name="create_developer", methods={"POST"})
     */
    public function create(Request $request)
    {
        $req = json_decode($request->getContent());

        $fname = $req->fname;
        $lname = $req->lname;
        $email = $req->email;
        $birthday = $req->birthday;
        $gender = $req->gender;
        $adresse = $req->adresse;
        $poste = $req->poste;
        $cin = $req->cin;
        $rib = $req->rib;
        $salaire = $req->salaire;
        $contrat = $req->contrat;
        /*  $file = "";
        if ($request->files) {
            $file =  $request->files->get('cv');
        } */


        if (!$fname) {
            return $this->json([
                "code" => 400,
                "msg" => "Verify your Credentials"
            ]);
        }



        $em = $this->getDoctrine()->getManager();


        $developer = new Developer();

        $developer->setFname($fname);
        $developer->setLname($lname);
        $developer->setEmail($email);
        $developer->setBirthday(new DateTime(date('Y-m-d', strtotime($birthday))));
        $developer->setGender($gender);
        $developer->setAdresse($adresse);
        $developer->setPoste($poste);
        $developer->setCin($cin);
        $developer->setRib($rib);
        $developer->setSalaire($salaire);
        $developer->setContrat($contrat);

        $developer->setCreatedAt(new DateTime());

        /*   $file =  $request->files->get('cv');
        $media = new MediaLibrary();
        $media->setCvName($file->getClientOriginalName());
        $media->setCvSize($file->getClientSize());
        $media->setCvFile($file);
        $em->persist($media);
 */

        $em->persist($developer);
        $em->flush();

        return $this->json([
            "code" => 200,
            "msg" => "Developer created succesfully!",
            "data" => $developer->getFname()
        ]);
    }

    /**
     * @Route("/api/developers/{id}", name="delete_developers", methods={"DELETE"})
     */
    public function delete($id)
    {
        $em = $this->getDoctrine()->getManager();
        $developer = $em->getRepository(Developer::class)->find($id);
        if (!$developer) {
            return $this->json([
                "msg" => "Developer does not exist!"
            ]);
        }
        $em->remove($developer);
        $em->flush();
        return $this->json([
            "code" => 200,
            "msg" => "Developer Deleted"
        ]);
    }

    /**
     * @Route("/api/developers/{id}", name="edit_developer", methods={"PUT"})
     */
    public function edit($id, Request $request)
    {
        $req = json_decode($request->getContent());

        $fname = $req->fname;
        $lname = $req->lname;
        $email = $req->email;
        $birthday = $req->birthday;
        $gender = $req->gender;
        $adresse = $req->adresse;
        $poste = $req->poste;
        $cin = $req->cin;
        $rib = $req->rib;
        $salaire = $req->salaire;
        $contrat = $req->contrat;

        if (!$fname) {
            return $this->json([
                "code" => 400,
                "msg" => "Verify your Credentials"
            ]);
        }

        $em = $this->getDoctrine()->getManager();
        $developer = $em->getRepository(Developer::class)->find($id);

        if (!$developer) {
            return $this->json([
                "msg" => "Developer does not exist"
            ]);
        }
        $developer->setFname($fname);
        $developer->setLname($lname);
        $developer->setEmail($email);
        $developer->setBirthday(new DateTime(date('Y-m-d', strtotime($birthday))));
        $developer->setGender($gender);
        $developer->setAdresse($adresse);
        $developer->setPoste($poste);
        $developer->setCin($cin);
        $developer->setRib($rib);
        $developer->setSalaire($salaire);
        $developer->setContrat($contrat);


        $em->flush();
        return $this->json([
            "code" => 200,
            "msg" => "Developer Updated"
        ]);
    }
}
