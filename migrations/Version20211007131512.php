<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20211007131512 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE conge_developer (conge_id INT NOT NULL, developer_id INT NOT NULL, INDEX IDX_1C683C40CAAC9A59 (conge_id), INDEX IDX_1C683C4064DD9267 (developer_id), PRIMARY KEY(conge_id, developer_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE conge_developer ADD CONSTRAINT FK_1C683C40CAAC9A59 FOREIGN KEY (conge_id) REFERENCES conge (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE conge_developer ADD CONSTRAINT FK_1C683C4064DD9267 FOREIGN KEY (developer_id) REFERENCES developer (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE conge DROP FOREIGN KEY FK_2ED8934864DD9267');
        $this->addSql('DROP INDEX IDX_2ED8934864DD9267 ON conge');
        $this->addSql('ALTER TABLE conge DROP developer_id');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP TABLE conge_developer');
        $this->addSql('ALTER TABLE conge ADD developer_id INT NOT NULL');
        $this->addSql('ALTER TABLE conge ADD CONSTRAINT FK_2ED8934864DD9267 FOREIGN KEY (developer_id) REFERENCES developer (id)');
        $this->addSql('CREATE INDEX IDX_2ED8934864DD9267 ON conge (developer_id)');
    }
}
