<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20211027092637 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE conge ADD CONSTRAINT FK_2ED8934864DD9267 FOREIGN KEY (developer_id) REFERENCES developer (id)');
        $this->addSql('CREATE INDEX IDX_2ED8934864DD9267 ON conge (developer_id)');
        $this->addSql('ALTER TABLE developer ADD poste VARCHAR(255) DEFAULT NULL, ADD cin VARCHAR(255) DEFAULT NULL, ADD rib VARCHAR(255) DEFAULT NULL, ADD salaire INT DEFAULT NULL, ADD contrat VARCHAR(255) DEFAULT NULL');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE conge DROP FOREIGN KEY FK_2ED8934864DD9267');
        $this->addSql('DROP INDEX IDX_2ED8934864DD9267 ON conge');
        $this->addSql('ALTER TABLE developer DROP poste, DROP cin, DROP rib, DROP salaire, DROP contrat');
    }
}
